$(document).ready(function() {
    $(window).bind('scroll', function(e) {
        parallax();
        hideme();
    });
});


function hideme(){
    $('.hideme').each( function(i){
        
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > bottom_of_object ){
            
            $(this).animate({'opacity':'1'},500);
                
        }
    });
}

function parallax() {
    var scrollPosition = $(window).scrollTop();
    var sWidth = $(window).width();
    $('.angle-sm-left').css('top',(150 - (scrollPosition * .1))+'px' );
    $('.angle-md').css('top',(280 - (scrollPosition * .4))+'px' );
    $('.angle-sm-right').css('top',(230 - (scrollPosition * .2))+'px' );
    $('.gray-slant').css('top',(150 - (scrollPosition * .3))+'px' );
    $('.burgline').css('top',(105 - (scrollPosition * .2))+'px' );
    if(sWidth<767){
        $('.para').css('top',(900 - (scrollPosition * .2))+'px' );
        $('.para2').css('top',(2150 - (scrollPosition * .35))+'px' );
    }else if(sWidth>=768 && sWidth<= 991){
        $('.para2').css('top',(1200 - (scrollPosition * .3))+'px' );
    }else{
        $('.para').css('top',(300 - (scrollPosition * .2))+'px' );
        $('.para2').css('top',(900 - (scrollPosition * .3))+'px' );
    }
    $('.gray-slant2').css('top',(2350 - (scrollPosition * .8))+'px' );
    $('.burgline2').css('top',(655 - (scrollPosition * .2))+'px' );
}